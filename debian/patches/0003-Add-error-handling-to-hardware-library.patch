From 2a62dc22a07ad19d18ed4510fbdb785e641ddc23 Mon Sep 17 00:00:00 2001
From: Andreas Pokorny <andreas.pokorny@gmail.com>
Date: Sun, 23 Dec 2018 22:13:48 +0100
Subject: [PATCH] Add error handling to hardware library

This addds _CHECKED variants of the macros in binding.h that only make sense when the
function signature allows and integer error return values.

Origin: vendor
Forwarded: no
Last-Update: 2020-12-10
---
 hybris/hardware/hardware.c             |   4 +-
 hybris/include/hybris/common/binding.h | 210 +++++++++++++++++++++++++
 utils/generate_wrapper_macros.py       |  20 +++
 3 files changed, 232 insertions(+), 2 deletions(-)

diff --git a/hybris/hardware/hardware.c b/hybris/hardware/hardware.c
index 0b19b6e..f9c2058 100644
--- a/hybris/hardware/hardware.c
+++ b/hybris/hardware/hardware.c
@@ -25,7 +25,7 @@
 HYBRIS_LIBRARY_INITIALIZE(hardware, "libhardware.so");
 #pragma GCC visibility pop
 
-HYBRIS_IMPLEMENT_FUNCTION2(hardware, int, hw_get_module, const char *, const struct hw_module_t **);
-HYBRIS_IMPLEMENT_FUNCTION3(hardware, int, hw_get_module_by_class, const char *, const char *, const struct hw_module_t **);
+HYBRIS_IMPLEMENT_FUNCTION_CHECKED2(hardware, int, hw_get_module, const char *, const struct hw_module_t **);
+HYBRIS_IMPLEMENT_FUNCTION_CHECKED3(hardware, int, hw_get_module_by_class, const char *, const char *, const struct hw_module_t **);
 
 // vim:ts=4:sw=4:noexpandtab
diff --git a/hybris/include/hybris/common/binding.h b/hybris/include/hybris/common/binding.h
index 85bcc5a..916f163 100644
--- a/hybris/include/hybris/common/binding.h
+++ b/hybris/include/hybris/common/binding.h
@@ -57,6 +57,16 @@ int android_dladdr(const void *addr, void *info);
         *(fptr) = (void *) android_dlsym(name##_handle, sym); \
     }
 
+#define HYBRIS_DLSYSM_CHECKED(name, fptr, sym) \
+    if (!name##_handle) \
+        hybris_##name##_initialize(); \
+    if (!name##_handle) \
+        return -EINVAL; \
+    if (*(fptr) == NULL) \
+    { \
+        *(fptr) = (void *) android_dlsym(name##_handle, sym); \
+    }
+
 #define HYBRIS_LIBRARY_INITIALIZE(name, path) \
     void *name##_handle; \
     void hybris_##name##_initialize() \
@@ -72,6 +82,16 @@ int android_dladdr(const void *addr, void *info);
 
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED0(name, return_type, symbol) \
+    return_type symbol() \
+    { \
+        static return_type (*f)() FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION0(name, return_type, symbol) \
     return_type symbol() \
     { \
@@ -81,6 +101,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED1(name, return_type, symbol, a1) \
+    return_type symbol(a1 n1) \
+    { \
+        static return_type (*f)(a1) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION1(name, return_type, symbol, a1) \
     return_type symbol(a1 n1) \
     { \
@@ -90,6 +120,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED2(name, return_type, symbol, a1, a2) \
+    return_type symbol(a1 n1, a2 n2) \
+    { \
+        static return_type (*f)(a1, a2) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION2(name, return_type, symbol, a1, a2) \
     return_type symbol(a1 n1, a2 n2) \
     { \
@@ -99,6 +139,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED3(name, return_type, symbol, a1, a2, a3) \
+    return_type symbol(a1 n1, a2 n2, a3 n3) \
+    { \
+        static return_type (*f)(a1, a2, a3) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION3(name, return_type, symbol, a1, a2, a3) \
     return_type symbol(a1 n1, a2 n2, a3 n3) \
     { \
@@ -108,6 +158,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED4(name, return_type, symbol, a1, a2, a3, a4) \
+    return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4) \
+    { \
+        static return_type (*f)(a1, a2, a3, a4) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3, n4); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION4(name, return_type, symbol, a1, a2, a3, a4) \
     return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4) \
     { \
@@ -117,6 +177,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED5(name, return_type, symbol, a1, a2, a3, a4, a5) \
+    return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5) \
+    { \
+        static return_type (*f)(a1, a2, a3, a4, a5) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3, n4, n5); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION5(name, return_type, symbol, a1, a2, a3, a4, a5) \
     return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5) \
     { \
@@ -126,6 +196,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED6(name, return_type, symbol, a1, a2, a3, a4, a5, a6) \
+    return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6) \
+    { \
+        static return_type (*f)(a1, a2, a3, a4, a5, a6) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3, n4, n5, n6); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION6(name, return_type, symbol, a1, a2, a3, a4, a5, a6) \
     return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6) \
     { \
@@ -135,6 +215,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED7(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7) \
+    return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7) \
+    { \
+        static return_type (*f)(a1, a2, a3, a4, a5, a6, a7) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3, n4, n5, n6, n7); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION7(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7) \
     return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7) \
     { \
@@ -144,6 +234,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED8(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8) \
+    return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8) \
+    { \
+        static return_type (*f)(a1, a2, a3, a4, a5, a6, a7, a8) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3, n4, n5, n6, n7, n8); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION8(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8) \
     return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8) \
     { \
@@ -153,6 +253,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED9(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9) \
+    return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9) \
+    { \
+        static return_type (*f)(a1, a2, a3, a4, a5, a6, a7, a8, a9) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3, n4, n5, n6, n7, n8, n9); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION9(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9) \
     return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9) \
     { \
@@ -162,6 +272,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED10(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10) \
+    return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10) \
+    { \
+        static return_type (*f)(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION10(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10) \
     return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10) \
     { \
@@ -171,6 +291,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED11(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11) \
+    return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11) \
+    { \
+        static return_type (*f)(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION11(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11) \
     return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11) \
     { \
@@ -180,6 +310,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED12(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12) \
+    return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11, a12 n12) \
+    { \
+        static return_type (*f)(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION12(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12) \
     return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11, a12 n12) \
     { \
@@ -189,6 +329,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED13(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13) \
+    return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11, a12 n12, a13 n13) \
+    { \
+        static return_type (*f)(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION13(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13) \
     return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11, a12 n12, a13 n13) \
     { \
@@ -198,6 +348,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED14(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14) \
+    return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11, a12 n12, a13 n13, a14 n14) \
+    { \
+        static return_type (*f)(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION14(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14) \
     return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11, a12 n12, a13 n13, a14 n14) \
     { \
@@ -207,6 +367,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED15(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15) \
+    return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11, a12 n12, a13 n13, a14 n14, a15 n15) \
+    { \
+        static return_type (*f)(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION15(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15) \
     return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11, a12 n12, a13 n13, a14 n14, a15 n15) \
     { \
@@ -216,6 +386,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED16(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16) \
+    return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11, a12 n12, a13 n13, a14 n14, a15 n15, a16 n16) \
+    { \
+        static return_type (*f)(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION16(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16) \
     return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11, a12 n12, a13 n13, a14 n14, a15 n15, a16 n16) \
     { \
@@ -225,6 +405,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED17(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17) \
+    return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11, a12 n12, a13 n13, a14 n14, a15 n15, a16 n16, a17 n17) \
+    { \
+        static return_type (*f)(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION17(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17) \
     return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11, a12 n12, a13 n13, a14 n14, a15 n15, a16 n16, a17 n17) \
     { \
@@ -234,6 +424,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED18(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18) \
+    return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11, a12 n12, a13 n13, a14 n14, a15 n15, a16 n16, a17 n17, a18 n18) \
+    { \
+        static return_type (*f)(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION18(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18) \
     return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11, a12 n12, a13 n13, a14 n14, a15 n15, a16 n16, a17 n17, a18 n18) \
     { \
@@ -243,6 +443,16 @@ int android_dladdr(const void *addr, void *info);
     }
 
 
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED19(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19) \
+    return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11, a12 n12, a13 n13, a14 n14, a15 n15, a16 n16, a17 n17, a18 n18, a19 n19) \
+    { \
+        static return_type (*f)(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19) FP_ATTRIB = NULL; \
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \
+        if (!f) \
+            return -EINVAL; \
+        return f(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19); \
+    }
+
 #define HYBRIS_IMPLEMENT_FUNCTION19(name, return_type, symbol, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19) \
     return_type symbol(a1 n1, a2 n2, a3 n3, a4 n4, a5 n5, a6 n6, a7 n7, a8 n8, a9 n9, a10 n10, a11 n11, a12 n12, a13 n13, a14 n14, a15 n15, a16 n16, a17 n17, a18 n18, a19 n19) \
     { \
diff --git a/utils/generate_wrapper_macros.py b/utils/generate_wrapper_macros.py
index 6b537ac..2500b5d 100644
--- a/utils/generate_wrapper_macros.py
+++ b/utils/generate_wrapper_macros.py
@@ -97,6 +97,16 @@ print """
         *(fptr) = (void *) android_dlsym(name##_handle, sym); \\
     }
 
+#define HYBRIS_DLSYSM_CHECKED(name, fptr, sym) \\
+    if (!name##_handle) \\
+        hybris_##name##_initialize(); \\
+    if (!name##_handle) \\
+        return -EINVAL; \\
+    if (*(fptr) == NULL) \\
+    { \\
+        *(fptr) = (void *) android_dlsym(name##_handle, sym); \\
+    }
+
 #define HYBRIS_LIBRARY_INITIALIZE(name, path) \\
     void *name##_handle; \\
     void hybris_##name##_initialize() \\
@@ -121,6 +131,16 @@ for count in range(MAX_ARGS):
     call_names = ', '.join(names)
 
     print """
+#define HYBRIS_IMPLEMENT_FUNCTION_CHECKED{count}({wrapper_signature}) \\
+    return_type symbol({signature_with_names}) \\
+    {BEGIN} \\
+        static return_type (*f)({signature}) FP_ATTRIB = NULL; \\
+        HYBRIS_DLSYSM_CHECKED(name, &f, #symbol); \\
+        if (!f) \\
+            return -EINVAL; \\
+        return f({call_names}); \\
+    {END}
+
 #define HYBRIS_IMPLEMENT_FUNCTION{count}({wrapper_signature}) \\
     return_type symbol({signature_with_names}) \\
     {BEGIN} \\
-- 
2.31.1

